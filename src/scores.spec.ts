import {
  getSimpleScoresBetweenDatesInAggregationOverall,
  getExistingScoresBetweenDatesInAggregationOverall
} from "./scores";

import {
  fixtureFull,
  fixtureNoAggregationOverall,
  fixtureNoScores,
  fixtureMissingInvalidScoreDate
} from "./fixtures";

describe("The getSimpleScoresBetweenDatesInAggregationOverall() function", () => {
  const expectedJSON = [
    { y: 282, x: "2015-08-19T14:00:19.352000Z" },
    { y: 227, x: "2015-10-08T14:45:31.991000Z" },
    { y: 185, x: "2015-10-12T07:27:47.493000Z" }
  ];

  test.each`
    input                          | startDate                        | endDate                          | expected        | expectedErr
    ${fixtureFull}                 | ${"2015-08-19T14:00:19.352000Z"} | ${"2015-10-12T07:27:47.493000Z"} | ${expectedJSON} | ${undefined}
    ${fixtureFull}                 | ${"2015-10-12T07:27:47.493000Z"} | ${"2015-08-19T14:00:19.352000Z"} | ${undefined}    | ${new Error("invalid dates")}
    ${fixtureFull}                 | ${"invalid date"}                | ${""}                            | ${undefined}    | ${new Error("invalid dates")}
    ${fixtureFull}                 | ${undefined}                     | ${"2015-08-19T14:00:19.352000Z"} | ${undefined}    | ${new Error("invalid dates")}
    ${fixtureNoAggregationOverall} | ${"2015-08-19T14:00:19.352000Z"} | ${"2015-10-12T07:27:47.493000Z"} | ${undefined}    | ${new Error("aggregation-overall not found")}
    ${fixtureNoScores}             | ${"2015-08-19T14:00:19.352000Z"} | ${"2015-10-12T07:27:47.493000Z"} | ${undefined}    | ${new Error("unavailable scores")}
  `(
    "should filter between $startDate and $endDate or return error $expectedErr",
    ({ input, startDate, endDate, expected, expectedErr }) => {
      const [output, err] = getSimpleScoresBetweenDatesInAggregationOverall(
        input,
        startDate,
        endDate
      );
      expect(output).toEqual(expected);

      expect(expectedErr).toEqual(err);
    }
  );
});

describe("The getExistingScoresBetweenDatesInAggregationOverall() function", () => {
  const expectedJSON = [
    { y: 282, x: "2015-08-19T14:00:19.352000Z" },
    { y: 227, x: "2015-10-08T14:45:31.991000Z" },
    { y: 185, x: "2015-10-12T07:27:47.493000Z" }
  ];
  const expectedJSONWhenInvalidDates = [
    { y: 282, x: "2015-08-19T14:00:19.352000Z" },
    { y: 171, x: "2015-11-13T09:39:16.580000Z" }
  ];
  test.each`
    input                             | startDate                        | endDate                          | expected                        | expectedErr
    ${fixtureFull}                    | ${"2015-08-19T14:00:00.000000Z"} | ${"2015-10-13T00:00:00.000000Z"} | ${expectedJSON}                 | ${undefined}
    ${fixtureFull}                    | ${"2015-10-13T00:00:00.000000Z"} | ${"2015-08-19T14:00:00.000000Z"} | ${undefined}                    | ${new Error("invalid dates")}
    ${fixtureFull}                    | ${"invalid date"}                | ${""}                            | ${undefined}                    | ${new Error("invalid dates")}
    ${fixtureFull}                    | ${undefined}                     | ${"2015-08-19T14:00:19.352000Z"} | ${undefined}                    | ${new Error("invalid dates")}
    ${fixtureNoAggregationOverall}    | ${"2015-08-19T14:00:00.000000Z"} | ${"2015-10-13T00:00:00.000000Z"} | ${undefined}                    | ${new Error("aggregation-overall not found")}
    ${fixtureNoScores}                | ${"2015-08-19T14:00:00.000000Z"} | ${"2015-10-13T00:00:00.000000Z"} | ${undefined}                    | ${new Error("unavailable scores")}
    ${fixtureMissingInvalidScoreDate} | ${"2015-08-19T14:00:00.000000Z"} | ${"2015-11-15T07:27:47.493000Z"} | ${expectedJSONWhenInvalidDates} | ${undefined}
  `(
    "should filter between $startDate and $endDate or return error $expectedErr",
    ({ input, startDate, endDate, expected, expectedErr }) => {
      const [output, err] = getExistingScoresBetweenDatesInAggregationOverall(
        input,
        startDate,
        endDate
      );
      expect(output).toEqual(expected);

      expect(expectedErr).toEqual(err);
    }
  );
});
