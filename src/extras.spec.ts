import {
  fixtureFull,
  fixtureNoExtras,
  fixtureNoAggregationOverall
} from "./fixtures";
import { getSimpleScoresBetweenDatesInAggregationOverall } from "./scores";
import { ScoresIndex, createScoresIndex } from "./extras";

describe("The createScoresIndex() function", () => {
  const expectedScore: ScoresIndex = {
    "2015-08-19T14:00:19.352000Z": {
      score: { x: "2015-08-19T14:00:19.352000Z", y: 282 },
      extra: {
        quiz_session_type: "Study",
        priority: 282,
        score_delta: null,
        quiz_session: 6775,
        quiz_config: 226,
        quiz_config_title: "Platform Reference for AWS"
      }
    },
    "2015-10-08T14:45:31.991000Z": {
      score: { x: "2015-10-08T14:45:31.991000Z", y: 227 },
      extra: {
        quiz_session_type: "Study",
        priority: 55,
        score_delta: -55,
        quiz_session: 19037,
        quiz_config: 226,
        quiz_config_title: "Platform Reference for AWS"
      }
    },
    "2015-10-12T07:27:47.493000Z": {
      score: { x: "2015-10-12T07:27:47.493000Z", y: 185 },
      extra: {
        quiz_session_type: "Study",
        priority: 42,
        score_delta: -42,
        quiz_session: 19337,
        quiz_config: 226,
        quiz_config_title: "Platform Reference for AWS"
      }
    }
  };

  test.each`
    input                          | startDate                        | endDate                          | expected         | expectedErr
    ${fixtureFull}                 | ${"2015-08-19T14:00:00.000000Z"} | ${"2015-10-13T00:00:00.000000Z"} | ${expectedScore} | ${undefined}
    ${fixtureNoAggregationOverall} | ${"2015-08-19T14:00:00.000000Z"} | ${"2015-10-13T00:00:00.000000Z"} | ${undefined}     | ${new Error("aggregation-overall not found")}
    ${fixtureNoExtras}             | ${"2015-08-19T14:00:00.000000Z"} | ${"2015-10-13T00:00:00.000000Z"} | ${undefined}     | ${new Error("unavailable extras")}
  `(
    "should create indexes for scores between $startDate and $endDate",
    ({ input, startDate, endDate, expected, expectedErr }) => {
      const [series] = getSimpleScoresBetweenDatesInAggregationOverall(
        input,
        startDate,
        endDate
      );
      const [output, err] = createScoresIndex(input, series);

      expect(output).toEqual(expected);

      expect(expectedErr).toEqual(err);
    }
  );
});
