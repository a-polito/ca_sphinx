import { RootObject } from "../model";

export const fixtureFull: RootObject = require("../../data.json");

export const fixtureNoAggregationOverall: RootObject = {
  data: [
    {
      details: [],
      title: "Non existent slug",
      type: "aggregation",
      id: 42,
      slug: "another-aggregation"
    }
  ]
};

export const fixtureNoScores: RootObject = {
  data: [
    {
      details: [
        {
          series: [
            {
              y: {
                quiz_session_type: "Study",
                priority: 282,
                score_delta: null,
                quiz_session: 6775,
                quiz_config: 226,
                quiz_config_title: "Platform Reference for AWS"
              },
              x: "2015-08-19T14:00:19.352000Z"
            },
            {
              y: {
                quiz_session_type: "Study",
                priority: 55,
                score_delta: -55,
                quiz_session: 19037,
                quiz_config: 226,
                quiz_config_title: "Platform Reference for AWS"
              },
              x: "2015-10-08T14:45:31.991000Z"
            }
          ],
          key: "extra"
        }
      ],
      title: "Overall",
      type: "aggregation",
      id: 42,
      slug: "aggregation-overall"
    }
  ]
};

export const fixtureNoExtras: RootObject = {
  data: [
    {
      details: [
        {
          series: [
            {
              y: 282,
              x: "2015-08-18T14:00:19.352000Z"
            },
            {
              y: 282,
              x: "2015-08-19T14:00:19.352000Z"
            },
            {
              y: 171,
              x: "2015-11-13T09:39:16.580000Z"
            },
            {
              y: 139,
              x: "2015-12-03T10:49:17.235000Z"
            }
          ],
          key: "score"
        }
      ],
      title: "Overall",
      type: "aggregation",
      id: 42,
      slug: "aggregation-overall"
    }
  ]
};

export const fixtureMissingInvalidScoreDate = {
  data: [
    {
      details: [
        {
          series: [
            {
              y: 282,
              x: "2015-08-18T14:00:19.352000Z"
            },
            {
              y: 282,
              x: "2015-08-19T14:00:19.352000Z"
            },
            {
              y: 227
            },
            {
              y: 185,
              x: ""
            },
            {
              y: 162,
              x: "not a date"
            },
            {
              y: 171,
              x: "2015-11-13T09:39:16.580000Z"
            },
            {
              y: 139,
              x: "2015-12-03T10:49:17.235000Z"
            }
          ],
          key: "score"
        }
      ],
      title: "Overall",
      type: "aggregation",
      id: 42,
      slug: "aggregation-overall"
    }
  ]
};
