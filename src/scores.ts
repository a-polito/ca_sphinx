import {
  RootObject,
  ScoreDetail,
  Datum,
  ExtraDetail,
  Series,
  Extra
} from "./model";

type FilterFn = (
  value: Series<number>,
  index?: number,
  array?: Series<number>[]
) => boolean;

/**
 * Performs a filter on the input without considering missing values
 * in the "x" of the "score" series, applying a simpler logic checking
 * if the date is between the start and end date
 * @param root: root object containing all input data
 * @param startDate: start date in string ISO-8601 formatted
 * @param endDate: end date in string ISO-8601 formatted
 * @returns an array tuple with always two values with:
 *    - the filtered items as first item
 *    - or the error as second item
 */
export function getSimpleScoresBetweenDatesInAggregationOverall(
  root: RootObject,
  startDate: string,
  endDate: string
): [Series<number>[], Error] {
  if (!areDatesValid(startDate, endDate)) {
    return [undefined, new Error("invalid dates")];
  }

  const start = new Date(startDate).getTime();
  const end = new Date(endDate).getTime();

  return getScoresInAggregationOverall(root, s => {
    const curr = new Date(s.x).getTime();
    return start <= curr && end >= curr;
  });
}

function areDatesValid(startDate: string, endDate: string): boolean {
  const start = new Date(startDate).getTime();
  const end = new Date(endDate).getTime();
  return !(end < start || isNaN(start) || isNaN(end));
}

/**
 * Performs a filter on the input considering missing or invalid values
 * in the "x" of the "scores" series, applying further checks to make it more
 * resilient
 * @param root: root object containing all inptut data
 * @param startDate: start date in string ISO-8601 formatted
 * @param endDate: end date in string ISO-8601 formatted
 * @returns an array tuple with always two values with:
 *    - the filtered items as first item
 *    - or the error as second item
 */
export function getExistingScoresBetweenDatesInAggregationOverall(
  root: RootObject,
  startDate: string,
  endDate: string
): [Series<number>[], Error] {
  if (!areDatesValid(startDate, endDate)) {
    return [undefined, new Error("invalid dates")];
  }

  const start = new Date(startDate).getTime();
  const end = new Date(endDate).getTime();

  return getScoresInAggregationOverall(root, s => {
    if (s.x) {
      const curr = new Date(s.x).getTime();
      return isNaN(curr) ? false : start <= curr && end >= curr;
    }
    return false;
  });
}

/**
 * Performs a filter operation, applying the given predicate function
 * on the scores extracted from the RootObject
 *
 * @param root: RootObject object root containing all data
 * @param matchFn: predicate function to be applied, to the list of data
 */
function getScoresInAggregationOverall(
  root: RootObject,
  matchFn: FilterFn
): [Series<number>[], Error] {
  const [aggregation]: Datum[] = getAggregationOverall(root);
  if (!aggregation) {
    return [undefined, new Error("aggregation-overall not found")];
  }

  const [scores] = getScores(aggregation);
  if (!scores) {
    return [undefined, new Error("unavailable scores")];
  }

  if (isScoreDetail(scores)) {
    const filteredScores = scores.series.filter(matchFn);

    return [filteredScores, undefined];
  }
}

function getAggregationOverall(root: RootObject) {
  return root.data.filter(d => d.slug === "aggregation-overall");
}

function getScores(aggregation: Datum) {
  return aggregation.details.filter(d => isScoreDetail(d));
}

/**
 * Type guarded function needed to detect if the given Detail is of
 * type ScoreDetail
 * @param detail
 */
function isScoreDetail(
  detail: ScoreDetail | ExtraDetail
): detail is ScoreDetail {
  return (detail as ScoreDetail).key === "score";
}
