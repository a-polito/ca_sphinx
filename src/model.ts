export interface Extra {
  quiz_session_type: string;
  priority: number;
  score_delta: number;
  quiz_session: number;
  quiz_config: number;
  quiz_config_title: string;
}
export interface Series<T> {
  y: T;
  x: string;
}

export interface ScoreDetail {
  series: Series<number>[];
  key: "score";
}
export interface ExtraDetail {
  series: Series<Extra>[];
  key: "extra";
}

export interface Datum {
  details: (ScoreDetail | ExtraDetail)[];
  title: string;
  type: string;
  id: number;
  slug: string;
}

export interface RootObject {
  data: Datum[];
}
