import {
  Series,
  Extra,
  RootObject,
  Datum,
  ScoreDetail,
  ExtraDetail
} from "./model";

export type ScoresIndex = {
  [key: string]: PlottableScore;
};

export interface PlottableScore {
  score?: Series<number>;
  extra?: Extra;
}

/**
 *
 * @param root: root object containing all data
 * @param series: score series already filtered with the data to be displayed
 * @returns an array tuple with always two values with:
 *    - the created index as first item
 *    - or the error as second item
 */
export function createScoresIndex(
  root: RootObject,
  series: Series<number>[]
): [ScoresIndex, Error] {
  const [aggregation]: Datum[] = getAggregationOverall(root);
  if (!aggregation) {
    return [undefined, new Error("aggregation-overall not found")];
  }

  const [extraDetail] = getExtras(aggregation);
  if (!extraDetail) {
    return [undefined, new Error("unavailable extras")];
  }

  if (isExtraDetail(extraDetail)) {
    const extras = extraDetail.series
      .filter(e => series.some(s => s.x === e.x))
      .reduce((acc: ScoresIndex, e) => {
        acc[e.x] = {
          extra: e.y
        };
        return acc;
      }, {});

    series.reduce((acc: ScoresIndex, s) => {
      if (acc[s.x]) {
        acc[s.x].score = s;
      }
      return acc;
    }, extras);
    return [extras, undefined];
  }

  return undefined;
}

function getAggregationOverall(root: RootObject) {
  return root.data.filter(d => d.slug === "aggregation-overall");
}

function getExtras(aggregation: Datum) {
  return aggregation.details.filter(d => isExtraDetail(d));
}

/**
 * Type guarded function needed to detect if the given Detail is of
 * type ExtraDetail
 * @param detail
 */
function isExtraDetail(
  detail: ScoreDetail | ExtraDetail
): detail is ExtraDetail {
  return (detail as ExtraDetail).key === "extra";
}
