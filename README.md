# Assignment

The data in the data.json file represent the data point to be plotted on a line graph.

Write a function that having a start_date and an end_date as input returns the subset of data included between the two for the slug ‘aggregation-overall’ and for the key ‘score’
Assume the start_date and end_date exactly match the “x” key in the serie
start_date and end_date must be included in the returned data.
The series always contains start_date and end_date
Input: start_date: "2015-08-19T14:00:19.352000Z" end_date: "2015-10-12T07:27:47.493000Z"

Expected result: json [ { "y":282, "x":"2015-08-19T14:00:19.352000Z" }, { "y":227, "x":"2015-10-08T14:45:31.991000Z" }, { "y":185, "x":"2015-10-12T07:27:47.493000Z" } ]

Write the same function as above to match the case that:
The series does not always contains end_date or start_date
Start_date and end_date don’t match the “x” key in the serie

Consider that we want to display the data with the key “extra” on mouse over on a point of the key “score”. Write a function to format the data for this use case.

## Implementation and execution

I have basically created 3 functions:

- `getSimpleScoresBetweenDatesInAggregationOverall` performs a filter on the input without considering missing values in the series

- `getExistingScoresBetweenDatesInAggregationOverall` performs a filter on the input considering missing values in the series, by making futher checks to make it more resilient

- `createScoresIndex` creates an index of scores and extras, indexed by date, in order to be able to immediately access them and extract the data to be displayed during the mouseover operation

Everyone of these functions returns a tuple of two items:

- the function result or undefined in case of errors
- an error if one occurs or undefined if all goes well.

So in order to use them effectively, you just need to do array destructuring to immediately get the results

The functions are exported and accessible from other libraries.

### Execution

Install the required the dependencies

```bash
$ npm install
```

Run the tests

```
$ npm test
```
